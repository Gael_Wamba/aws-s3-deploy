import os
from random import randint
import shutil

import requests

from bitbucket_pipes_toolkit.test import PipeTestCase


S3_BUCKET = f"bbci-pipes-test-s3-{os.getenv('BITBUCKET_BUILD_NUMBER')}"
S3_BUCKET_TEST = f"{S3_BUCKET}/test"


class S3DeployTestCase(PipeTestCase):

    def setUp(self):
        super().setUp()
        self.random_number = randint(0, 32767)
        self.local_dir = f'aws-s3-deploy-test-{self.random_number}'
        os.mkdir(self.local_dir)

        self.filename = f'{self.local_dir}/deployment-{self.random_number}.txt'

        with open(self.filename, 'w+') as deployment_file:
            deployment_file.write('Pipelines is awesome!')

    def tearDown(self):
        shutil.rmtree(self.local_dir, ignore_errors=True)

    def test_upload_to_s3_bucket(self):
        region = os.getenv('AWS_DEFAULT_REGION', 'us-east-1')

        self.run_container(environment={
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_DEFAULT_REGION': region,
            'S3_BUCKET': S3_BUCKET_TEST,
            'ACL': "public-read",
            'LOCAL_PATH': self.local_dir
        })

        response = requests.get(f'https://{S3_BUCKET}.s3.{region}.amazonaws.com/test/deployment-{self.random_number}.txt')
        self.assertIn('Pipelines is awesome!', response.content.decode())

    def test_should_fail_if_local_path_doent_exist(self):

        result = self.run_container(environment={
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_DEFAULT_REGION': os.getenv('AWS_DEFAULT_REGION', 'us-east-1'),
            'S3_BUCKET': S3_BUCKET_TEST,
            'ACL': "public-read",
            'LOCAL_PATH': 'not_exist_local_path'
        })

        self.assertIn('not_exist_local_path directory doesn\'t exist', result)

    def test_success_update_oidc_authentication(self):
        region = os.getenv('AWS_DEFAULT_REGION', 'us-east-1')
        result = self.run_container(
            environment={
                'BITBUCKET_STEP_OIDC_TOKEN': os.getenv('BITBUCKET_STEP_OIDC_TOKEN'),
                'AWS_OIDC_ROLE_ARN': os.getenv('AWS_OIDC_ROLE_ARN'),
                'AWS_DEFAULT_REGION': region,
                'S3_BUCKET': S3_BUCKET_TEST,
                'ACL': 'public-read',
                'LOCAL_PATH': self.local_dir
            }
        )
        self.assertIn('Authenticating with a OpenID Connect (OIDC) Web Identity Provider', result)
        self.assertIn('✔ Deployment successful', result)
        response = requests.get(
            f'https://{S3_BUCKET}.s3.{region}.amazonaws.com/test/deployment-{self.random_number}.txt')
        self.assertIn('Pipelines is awesome!', response.content.decode())

    def test_success_update_fallback_to_default_authentication(self):
        region = os.getenv('AWS_DEFAULT_REGION', 'us-east-1')
        result = self.run_container(
            environment={
                'AWS_OIDC_ROLE_ARN': os.getenv('AWS_OIDC_ROLE_ARN'),
                'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
                'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
                'AWS_DEFAULT_REGION': region,
                'S3_BUCKET': S3_BUCKET_TEST,
                'ACL': 'public-read',
                'LOCAL_PATH': self.local_dir
            }
        )
        self.assertIn('Parameter `oidc: true` in the step configuration is required for OIDC authentication', result)
        self.assertIn('Using default authentication with AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY.', result)
        self.assertIn('✔ Deployment successful', result)
        response = requests.get(
            f'https://{S3_BUCKET}.s3.{region}.amazonaws.com/test/deployment-{self.random_number}.txt')
        self.assertIn('Pipelines is awesome!', response.content.decode())

    def test_should_fail_if_local_is_not_a_directory(self):

        result = self.run_container(environment={
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_DEFAULT_REGION': os.getenv('AWS_DEFAULT_REGION', 'us-east-1'),
            'S3_BUCKET': S3_BUCKET_TEST,
            'ACL': "public-read",
            'LOCAL_PATH': self.filename
        })

        self.assertIn(f'{self.filename} directory doesn\'t exist', result)

    def test_success_with_preexecution_hook(self):
        region = os.getenv('AWS_DEFAULT_REGION', 'us-east-1')

        preexecution = 'test/.preexecution-hook.sh'
        with open(preexecution, 'w') as f:
            f.write('aws configure set max_queue_size 500')
        os.chmod(preexecution, 0o0005)

        result = self.run_container(
            environment={
                'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
                'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
                'AWS_DEFAULT_REGION': region,
                'S3_BUCKET': S3_BUCKET_TEST,
                'BITBUCKET_PIPE_SHARED_STORAGE_DIR': os.getenv('BITBUCKET_PIPE_SHARED_STORAGE_DIR'),
                'ACL': 'public-read',
                'PRE_EXECUTION_SCRIPT': preexecution,
                'LOCAL_PATH': self.local_dir,
                'DEBUG': True
            }
        )
        self.assertIn('✔ Deployment successful', result)
        response = requests.get(
            f'https://{S3_BUCKET}.s3.{region}.amazonaws.com/test/deployment-{self.random_number}.txt')
        self.assertIn('Pipelines is awesome!', response.content.decode())
